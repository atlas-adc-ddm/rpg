# Interface to CRIC data. Used for getting site and blacklisting information.

from datetime import datetime
import json
import logging
import re
import requests

class CRICException(Exception):
    pass

logger = logging.getLogger("CRICHandler")

def _getCRICInfo(url, cache, certificate=None):
    '''
    Try to download json from the url and save to cache. If download fails use
    previously cached data. If that fails raise CRICException. Returns a
    dictionary of the parsed json
    '''
    try:
        data = requests.get(url, cert=certificate).json()
    except Exception as e:
        logger.warn("Failed to get CRIC info from {}: {}".format(url, str(e)))
        try:
            with open(cache) as data_file:
                data = json.load(data_file)
            logger.info("Loaded CRIC info from local cache")
        except Exception as e:
            raise CRICException("Failed to get blacklisting information from CRIC or local cache")
    else:
        # dump info to cache
        with open(cache, 'w') as data_file:
            json.dump(data, data_file)

    return data

def _filterCRICInfo(data, sites, token):
    '''
    Do the filtering of data
    '''
    for site in sites[:]:
        cricsite = site + '_' + token
        # Expand Tx
        if re.match('T[0123]$', site):
            tiersites = [ep for (ep, info) in data.items() if re.search('_%s$' % token, ep) and \
                                                              info['tier_level'] == int(site[1]) and \
                                                              info['state'] == 'ACTIVE' and not \
                                                              re.search('MWTEST|RUCIOTEST', ep)]
            tiersites = [re.sub('_%s$' % token, '', s) for s in tiersites]
            logger.info("Expanding {} to {}".format(site, tiersites))
            sites.remove(site)
            sites.extend(tiersites)
            continue
        # Remove non-existing sites
        if cricsite not in data:
            logger.warning('{} does not exist in CRIC, will not use'.format(cricsite))
            sites.remove(site)
            continue
        # Remove disabled sites
        if data[cricsite].get('state') == 'DISABLED':
            logger.warning('{} is DISABLED in CRIC, will not use'.format(cricsite))
            sites.remove(site)

def getDDMEndpoints(cachedir, srcsites, destsites, srctoken, desttoken, certificate=None):
    '''
    Take the lists of sources and destinations sites, expand groupings like T1
    and remove disabled or non-existing sites.
    '''

    url = "https://atlas-cric.cern.ch/api/atlas/ddmendpoint/query/?json"
    cache = "/".join([cachedir, "cric-ddmendpoint.data"])
    data = _getCRICInfo(url, cache, certificate)

    _filterCRICInfo(data, srcsites, srctoken)
    _filterCRICInfo(data, destsites, desttoken)


def getBlacklistedEndpoints(cachedir, certificate=None):
    '''
    Get the list of currently blacklisted endpoints from CRIC. Returns a tuple
    of ([blacklisted reading], [blacklistwriting]). Caches data in the specified
    cache dir and uses this if CRIC is unavailable.
    '''

    url = "https://atlas-cric.cern.ch/api/atlas/ddmendpointstatus/query/?json"
    cache = "/".join([cachedir, "agis-blacklisting.data"])
    data = _getCRICInfo(url, cache, certificate)

    blacklistedreading = []
    blacklistedwriting = []

    for endpoint, operations in data.items():
        if 'read_wan' in operations and operations['read_wan']['status']['value'] == 'OFF':
            # check it is still valid
            exptime = operations['read_wan']['status']['expiration']
            if not exptime or datetime.strptime(exptime[:19], "%Y-%m-%dT%H:%M:%S") > datetime.utcnow():
                blacklistedreading.append(endpoint)
        if 'write_wan' in operations and operations['write_wan']['status']['value'] == 'OFF':
            exptime = operations['write_wan']['status']['expiration']
            if not exptime or datetime.strptime(exptime[:19], "%Y-%m-%dT%H:%M:%S") > datetime.utcnow():
                blacklistedwriting.append(endpoint)

    logger.info("CRIC endpoints blacklisted for read: {}".format(blacklistedreading))
    logger.info("CRIC endpoints blacklisted for write: {}".format(blacklistedwriting))
    return (blacklistedreading, blacklistedwriting)
