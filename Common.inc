# this is a common configuration file that must be included in all other configurations

# Add this section to disable subscriptions being made during debugging
#[test]

[log]
log_dir = /data/ddmusr01/log
log_level = DEBUG

[cache]
cache_dir_base = /data/ddmusr01/tmp/rpg_cache
cache_dir_common = common

[policy]
email = alexander.bogdanchikov@cern.ch fabio.luchetti@cern.ch


