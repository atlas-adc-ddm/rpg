# Check process is not already running
if [ `ps aux|grep -c "RPG.py $@"` -gt 1 ]; then
  echo "Already running"
  exit 0
fi
# Get the keytab to access proxy on afs
kinit -kt /data/ddmusr01/keytab ddmusr01@CERN.CH
cd /data/ddmusr01/rpg
export X509_USER_PROXY=/data/ddmusr01/ddmusr01_latest_x509up.rfc.proxy
export RUCIO_ACCOUNT=ddmadmin
# Silence rucio setup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q -3
# Set system CAs for requests
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-bundle.crt
lsetup rucio -q
./RPG.py $@

