#!/usr/bin/env python3
# This file contains implementation of ConfigParseExtended class derived from ConfigParser to extend its functionality.
# The only implemented feature right now (May 2022) is reading of configurations files tree.
# Each config file may contain include directive with a list of config files to be read before this file.
# Extended tree reading is implemented in read_extended() method. It pre-parses config files to read config files
# sequentially according to include hierarchy.
#
# __________________________________________________________________
# For example, to add b.conf and c.conf before a.conf:
# Add the following instruction to a.conf:
# [include]
#    files = b.conf c.conf
#
# The including order in the string is left to right, can be any depth, cycling is resolved.
#
# To read whole configuration use the method ConfigParserExtended.read_extended()
# instead of ConfigParser.read():
#
#   import ConfigParserExtended as ce
#   c = ce.ConfigParserExtended()
#   c.read_extended("a.conf")
#
# thus, the configuration files will be read in the following order: b.conf, c.conf, a.conf
# -------------------------------------------------------------------
#
# Developed in context of RPG (DDM-ADC-ATLAS-CERN) project
# Author:
#  - Alexander Bogdanchikov, 2022

import configparser


class ConfigParserExtended(configparser.ConfigParser):
    def __init__(self):
        self.conf_names_all = None
        configparser.ConfigParser.__init__(self)

    @staticmethod
    def get_inc_list(config_file_name: str) -> list:
        cp = configparser.ConfigParser()
        if not cp.read(config_file_name):
            raise Exception("Cannot read configuration from \"%s\" file" % config_file_name)
        return cp.get('include', 'files').split() if cp.has_option('include', 'files') else []

    @staticmethod
    def update_inc_files(config_file_name: str, inc_files=None) -> list:
        if inc_files is None:
            inc_files = []
        pos = 0  # set position of current top file
        inc_files.insert(pos, config_file_name)  # supposed here that config_file_name is not in the inc_files list yet
        for fc in ConfigParserExtended.get_inc_list(config_file_name):
            if fc not in inc_files:
                inc_files = ConfigParserExtended.update_inc_files(fc, inc_files)
            else:  # rearrange position if necessary
                fc_pos = inc_files.index(fc)
                if pos < fc_pos:  # move current top file after included
                    inc_files.insert(fc_pos, inc_files.pop(pos))
                    pos = fc_pos
        return inc_files

    def read_extended(self, conf_names):
        conf_names_all = []
        if isinstance(conf_names, str):  # if conf_names is a string
            conf_names_all = ConfigParserExtended.update_inc_files(conf_names)
        else:  # if conf_names is a list
            conf_names_raw = []
            for conf_name in conf_names:
                conf_names_raw += ConfigParserExtended.update_inc_files(conf_name)
            # leave uniq names, saving the order when lookup from the end
            for conf_name in reversed(conf_names_raw):
                if conf_name not in conf_names_all:
                    conf_names_all.insert(0, conf_name)
        self.conf_names_all = conf_names_all
        return self.read(conf_names_all)


if __name__ == '__main__':
    c = ConfigParserExtended()
    c.read_extended(["a.conf", "d.conf"])
    print({section: dict(c[section]) for section in c.sections()})
    print("all config files: %s" % c.conf_names_all)
    c.read_extended("a.conf")
    print("all config files: %s" % c.conf_names_all)
    print({section: dict(c[section]) for section in c.sections()})
