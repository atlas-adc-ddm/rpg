#!/usr/bin/env python3
#
# Replication Policy on the Grid: Replicate data using Rucio rules.
# Originally written by Stephane Jezequel for migrating data to tape, rewritten
# in python and for generic data movement by David Cameron
#
# Tested with python 3.6
#
# Logic:
# - loop over destination sites and collect current rules (locks)
# - loop over source sites and collect datasets with specified patterns
# - filter out datasets with enough copies done or currently copying
# - filter out datasets using metadata criteria (eg updated_at time)
# - calculate destination share of rules based on configured policy (eg MoU)
# - assign datasets to sites
# -- if prefer_local is true, subscribe to own destination until max reached
# -- subscribe randomly to other sites until max reached
#
# Authors:
# - David Cameron, 2015-2022
# - Alexander Bogdanchikov, 2021-2022

# TODO:
# - use rucio RSE expressions/attributes for weighted data distribution
import configparser
import ConfigParserExtended as cp
import glob
import itertools
import json
import logging
import os
import random
import re
import smtplib
import sys
import time
import types
from collections import defaultdict
from datetime import date, datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from types import MethodType
from getpass import getuser
from socket import getfqdn

import fastavro
import requests

import CRICHandler
from rucio.client import Client
from rucio.common.exception import RucioException, DuplicateRule, DataIdentifierNotFound


class Debug:
    # Definition of variables to OVERRIDE values in CONFIG FILES for debugging
    # When commented, the configuration file will be used (comment for normal RPG behavior)
    #
    # define test mode regardless what is in config file
    # test = True  # test mode: do not apply generated rules
    #
    #  define testers email(s) and ignore "email" setting in conf files
    # email = ['Alexander.Bogdanchikov@cern.ch']
    #
    # set log_level regardless of conf file setting ([log] log_level)
    # log_level = 'DEBUG'

    # Debug variables below are not presented in config files:
    #
    # switch off dump usage (old behaviour) which is enabled by default
    # dump_force = None  # disable dump usage
    #
    # fallback to rucio calls if dump failed.
    # rucio_fallback = True
    #
    # allow_use_old_dump_hours>0 (<9) allows using yesterday dump if today dump is not available (when exec at night)
    # allow_use_old_dump_hours = 5
    #
    # send emails even if no rules have been created
    # email_empty=True
    email_empty = True


c = Client()


class RPGException(Exception):
    """
    Exception thrown when something stops any further processing.
    """
    pass


# decorator to log method execution time
def log_time(method):
    def inner(self, *args, **kwargs):
        self.logger.debug('%s.%s%r%r started' % (type(self).__name__, method.__name__, args, kwargs))
        start_time = time.time()
        result = method(self, *args, **kwargs)
        end_time = time.time()
        self.logger.debug('%s.%s%r%r finished in %.3f s' %
                          (type(self).__name__, method.__name__, args, kwargs, end_time - start_time))
        return result
    return inner


def utc_diff() -> str:
    """ return str - difference to UTC time """
    # hack to get difference with UTC time (timezone+DST)
    ts = time.time()
    utc_offset = (datetime.fromtimestamp(ts) -
                  datetime.utcfromtimestamp(ts)).total_seconds()
    return time.strftime("UTC+%H:%M", time.gmtime(utc_offset))


class CachedDataHandler:
    """
    Class for handling lists of datasets stored in files.
    """

    def __init__(self, filename):
        self.filename = filename
        self.data = set()
        self.logger = logging.getLogger("CachedDataHandler")
        if not os.path.exists(self.filename):
            try:
                open(self.filename, 'w').close()
            except IOError:
                os.makedirs(os.path.dirname(self.filename), 0o755)
                open(self.filename, 'w').close()
        else:
            try:
                self.data = set(line.strip() for line in open(self.filename))
            except Exception as e:
                self.logger.warning("Failed opening file %s: %s", self.filename, str(e))

    def getEntries(self):
        return self.data

    def hasEntry(self, entry):
        return entry in self.data

    def addEntry(self, entry):
        if entry:
            self.addEntries([entry])

    def addEntries(self, entries):
        if entries:
            with open(self.filename, 'a') as f:
                for entry in entries:
                    f.write(entry + '\n')
                    self.data.add(entry)


class DumpForce:
    """
    this class provides replacement of calls to the server with data from the dump.
    The replacement is by no means a full-fledged replacement, since the field in the dump is different.
    """

    # the constants' correspondence are taken from rucio.db.sqla.constants and RuleState
    state_map = {'R': 'REPLICATING', 'O': 'OK', 'S': 'STUCK',
                 'U': 'SUSPENDED', 'W': 'WAITING_APPROVAL', 'I': 'INJECT'}

    bad_dump_ext = '.bad'

    def __init__(self, cache_dir_common):
        self.logger = logging.getLogger('DumpForce')
        self.rules_file_path = None
        self.last_rse_rules = None

        # check/create cache_dir_common directory
        try:
            if not os.path.isdir(cache_dir_common):
                self.logger.warning("Failed to find, try to create cache_dir_common %s", cache_dir_common)
                os.makedirs(cache_dir_common, 0o755)
            if not os.access(cache_dir_common, os.R_OK | os.W_OK):
                self.logger.warning("Failed to access cache_dir_common %s", cache_dir_common)
                raise IOError
            self.cache_dir_common = cache_dir_common
        except Exception as ex:
            self.logger.warning("unhandled exception when try to get/create cache_dir_common %s: %s"
                                % (cache_dir_common, str(ex)))
            raise ex

    def _remove_rules_dumps(self, exclude=None):
        """
        removes rules dump from self.cache_dir_common directory excluding some files
        :exclude: list of files (by full path) to exclude from removal
        :return:
        """
        exclude = [] if exclude is None else exclude  # avoid "using a mutable default value as an argument"
        match_good = self.cache_dir_common + "/" + 'rules_????-??-??'
        match_bad = match_good + self.bad_dump_ext
        flist = glob.glob(match_good) + glob.glob(match_bad)
        rem_list = [f for f in flist if f not in exclude]
        count = 0
        for fpath in rem_list:
            try:
                os.remove(fpath)
                count += 1
            except Exception as ex:
                self.logger.warning("Exception while deleting file %s: %s" % (fpath, str(ex)))
        return count

    def fresh_rules_exist(self, rules_file_name=None, days_depth=1, move_on_failure=True) -> bool:
        """
        find if new rules changes exist in rules_file_name
        parameter rules_file_name:
            None: get from self.rules_file_path
        parameter days_depth:
            0: today changes
            1: not more than prev 1 day changes: yesterday (today and yesterday changes)
            2: not more than prev 2 days old changes
            ...
        parameter move_on_failure:
            True: if search failed or none fresh rules found, then rename: add "bad file" extension to the dump file
            False: do nothing with rules file on failure
        """
        rules_file = rules_file_name if rules_file_name is not None else self.rules_file_path
        if rules_file is None:
            self.logger.warning("can not check dump freshness: rules dump file is not specified (None)")
            return False
        # get min_date of rule change date to search
        min_date = int((date.today() - timedelta(days=days_depth)).strftime('%Y%m%d'))  # as int (i.e. 20220519)
        try:
            with open(rules_file, 'rb') as f:
                reader = fastavro.reader(f)
                for record in reader:
                    for fn in ["UPDATED_AT", "CREATED_AT", "STUCK_AT"]:  # , "EXPIRES_AT" ]:
                        if record[fn] is not None:
                            date_in_record = int(datetime.utcfromtimestamp(record[fn]/1000).strftime('%Y%m%d'))
                            if date_in_record >= min_date:
                                self.logger.info("fresh rules (%s %d>= %d) found in %s"
                                                 % (fn, date_in_record, min_date, rules_file))
                                # print("Found: ", fn, date_in_record, record)
                                return True
        except Exception as ex:
            self.logger.warning("exception when looking for fresh rules records in the file %s: %s"
                                % (rules_file, str(ex)))
        self.logger.warning("no fresh rules (>= %d) found in %s" % (min_date, rules_file))
        if move_on_failure:
            os.rename(rules_file, rules_file+self.bad_dump_ext)  # rename
            self.logger.warning("flag move_on_failure is set: rules file was moved: %s" % rules_file)
            if rules_file_name is None:  # if self.rules_file_path was used, then mark it as absent
                self.rules_file_path = None
        return False

    def rules_dump_available(self, timeout=(20., 20.)):
        """
        Check if fresh dump file exists in the cache dir, else try to download it
        """
        try:
            # check if the get_rules_dump was called successfully already
            if self.rules_file_path is not None:
                return True
            # compose the expected (fresh) dump filename / filepath
            lfile = datetime.now().strftime('rules_%Y-%m-%d')
            lpath = self.cache_dir_common + "/" + lfile  # cahedircommon/rules_2021-11-29
            lfile_old = None
            if 'allow_use_old_dump_hours' in Debug.__dict__ and 9 > Debug.allow_use_old_dump_hours > 0:
                prev_file = (datetime.now() - timedelta(hours=Debug.allow_use_old_dump_hours)).strftime(
                    'rules_%Y-%m-%d')
                if prev_file != lfile:
                    lfile_old = prev_file
            # check if the file exist (if left from previous run)
            if os.path.isfile(lpath):
                if os.access(lpath, os.R_OK):  # if enough access to read
                    self.rules_file_path = lpath
                    return True
                else:  # file exists but without read access, try to solve this by removing file
                    self.logger.warning("rules dump %s exists but has no read access => try to remove the file" % lpath)
                    os.remove(lpath)
            # try to download file
            url = 'http://atlas-rucio-analytix-02.cern.ch:8080/DUMPS/GetFileFromHDFS?dump=rules'
            # get info about the dump file
            with requests.Session() as session:
                try:
                    resp = session.head(url, timeout=timeout)
                except Exception as ex:
                    self.logger.warning("requested HEAD url=%s exception: %s" % (url, str(ex)))
                    return False
                if resp.status_code != 200:
                    self.logger.warning("failed HEAD request (%d response) url=%s" % (resp.status_code, url))
                    return False
                # parse header and check if filename is expected filename
                s = resp.headers[
                    'Content-Disposition']  # e.g. Content-Disposition: attachment;filename=rules_2021-12-05
                rfile = re.search(r"filename=(rules_\d{4}-\d{2}-\d{2})", s).group(1)
                if rfile != lfile:
                    self.logger.warning("no fresh dump file at %s: %s when expected %s" % (url, rfile, lfile))
                    # if old dump usage is acceptable check its availability
                    if lfile_old is not None:
                        lfile = lfile_old
                        self.logger.info("try to use old dump %s (allowed till %d hour)"
                                         % (lfile, Debug.allow_use_old_dump_hours))
                        lpath = self.cache_dir_common + "/" + lfile
                        # if enough access to read
                        if os.path.isfile(lpath) and os.access(lpath, os.R_OK):
                            self.rules_file_path = lpath
                            return True
                        if rfile != lfile:
                            self.logger.info("old dump %s is not available also" % lfile)
                            return False
                    else:
                        return False
                # remove old dump files before downloading a new one
                # ! leave yesterday dump since it may be other RPG scripts started yesterday and not finished yet
                ypath = self.cache_dir_common + "/" + (datetime.now() - timedelta(days=1)).strftime('rules_%Y-%m-%d')
                removed_counter = self._remove_rules_dumps(
                    exclude=[lpath, ypath, lpath+self.bad_dump_ext, ypath+self.bad_dump_ext])
                if removed_counter > 0:
                    self.logger.debug("%d old rules dumps were removed" % removed_counter)
                # get dump file itself
                try:
                    with session.get(url, stream=True, timeout=timeout) as resp:
                        resp.raise_for_status()
                        with open(lpath, 'wb') as f:
                            for chunk in resp.iter_content(chunk_size=16 * 1024):
                                f.write(chunk)
                                # last_pos=f.tell()
                except Exception as ex:
                    self.logger.warning("GET url=%s, exception: %s" % (url, str(ex)))
                    try:
                        os.remove(lpath)  # try to remove (the partially loaded file)
                    except Exception:  # file may not exist
                        pass
                    return False
                lsize = os.path.getsize(lpath)
                try:
                    rsize = int(resp.headers['Content-Length'])
                    if lsize != rsize:
                        self.logger.warning("remote size != local_size: %d != %d (url=%s local=%s)"
                                            % (rsize, lsize, url, lpath))
                        return False
                except KeyError as ex:
                        self.logger.warning("%s" % (str(ex)))
                self.logger.info("rules dump file %s (%d bytes) was received" % (lpath, lsize))
                self.rules_file_path = lpath
                return True
        except Exception as ex:
            self.logger.warning("exception in get_rules_dump(): %s" % (str(ex)))
        return False

    # check if function has dump presentation
    def func_available(self, obj, func):
        """
        returns True if the func is known (has read-dump implementation)
        """
        if isinstance(obj, Client):  # rucio.client.client.Client
            implemented_func = ['get_dataset_locks_by_rse', 'get_replication_rule']
            if func.__name__ in implemented_func:
                return True
        return False

    @log_time
    def read_dump(self):
        self.scope = []
        self.name = []
        self.state = []
        self.rule_id = []
        self.locks_replicating_cnt = []
        self.locks_stuck_cnt = []
        self.rse = []
        with open(self.rules_file_path, 'rb') as f:
            rd = fastavro.reader(f)  # can not be called twice for one descriptor
            for rec in rd:
                self.scope.append(rec['SCOPE'])
                self.name.append(rec['NAME'])
                self.state.append(DumpForce.state_map[rec['STATE']])
                self.rule_id.append(rec['ID'].lower())
                self.locks_replicating_cnt.append(int(rec['LOCKS_REPLICATING_CNT']))
                self.locks_stuck_cnt.append(int(rec['LOCKS_STUCK_CNT']))
                self.rse.append(rec['RSE_EXPRESSION'])

    @log_time
    def exec(self, obj, func, *args, **kwargs):
        if isinstance(obj, Client):
            if func.__name__ == 'get_dataset_locks_by_rse':
                rse = args[0]  # e.g. 'BNL-OSG2_DATATAPE'
                if 'rule_id' not in self.__dict__:  # if dump is loaded in memory
                    self.read_dump()
                keys = ['scope', 'name', 'state', 'rule_id', 'locks_replicating_cnt', 'locks_stuck_cnt']
                for i, rs in enumerate(self.rse):
                    if rs == rse and 'test' not in self.scope[i]:
                        yield (
                            dict(zip(keys, (self.scope[i], self.name[i], self.state[i], self.rule_id[i],
                                            self.locks_replicating_cnt[i], self.locks_stuck_cnt[i]))))
                return
            elif func.__name__ == 'get_replication_rule':
                rule_id = args[0]
                with open(self.rules_file_path, 'rb') as f:
                    rd = fastavro.reader(f)  # can not be called twice for one descriptor
                    lr = \
                        [{'scope': rec['SCOPE'],
                          'name': rec['NAME'],
                          'state': DumpForce.state_map[rec['STATE']],
                          'rule_id': rec['ID'].lower(),
                          'locks_replicating_cnt': int(rec['LOCKS_REPLICATING_CNT']),
                          'locks_stuck_cnt': int(rec['LOCKS_STUCK_CNT'])
                          }
                         for rec in rd if rec['ID'] == rule_id]
                    if len(lr) == 1:
                        return lr[0]
        return None


class Wrapper:
    """
    Wrapper to Rucio calls.
    * Catches exceptions and handles retries in a
    rather simple way, just repeating the call-up to 3 times. If a call takes
    longer than timeout, it will try again.
    * If dump_force is on then uses dump file instead of "known" Rucio calls
    """

    def __init__(self, other, name, timeout, dump_force=None):
        self.other = other
        self.timeout = timeout
        self.logger = logging.getLogger(name)
        self.dump_force = dump_force

    def __getattr__(self, name):
        if hasattr(self.other, name):
            func = getattr(self.other, name)
            return lambda *args, **kwargs: self._wrap(func, args, kwargs)
        raise AttributeError(name)

    def _wrap(self, func, args, kwargs):
        result = None
        # if dump usage is on
        if self.dump_force is not None and self.dump_force.func_available(self.other, func):
            if not self.dump_force.rules_dump_available():
                reason = "unable to get dump"
            else:
                if not self.dump_force.fresh_rules_exist():
                    reason = "no fresh rules in the dump"
                else:
                    result = self.dump_force.exec(self.other, func, *args, **kwargs)
                    if result is not None:
                        return result
                    reason = "failed to get %s() data from dump" % str(func.__name__)
            self.logger.warning("failed to use dump: %s" % reason)
            if 'rucio_fallback' not in Debug.__dict__ or not Debug.rucio_fallback:
                self.logger.error("Throw RPG exception: dump use failed when Rucio-calls fallback is disabled")
                raise RPGException("Dump usage failed (%s) and Rucio-calls usage fallback is disabled" % reason)
        # rucio calls
        for attempt in range(3):
            start_time = time.time()
            try:
                if type(func) == MethodType:
                    result = func(*args, **kwargs)
                else:
                    result = func(self.other, *args, **kwargs)
            except DuplicateRule:
                raise
            except DataIdentifierNotFound:
                raise
            except RucioException as e:
                self.logger.error("Rucio exception: %s (attempt %i)", str(e), attempt + 1)
            except Exception as e:
                self.logger.error("Unexpected exception: %s (attempt %i)", str(e), attempt + 1)
            else:
                if isinstance(result, types.GeneratorType):
                    # Stream whole result here to enforce timeout
                    try:
                        result = [i for i in result]
                    except (requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError) as e:
                        self.logger.warning(
                            "Error while streaming result from Rucio (attempt %d): %s" % (attempt + 1, str(e)))
                        result = None
                        continue
                if self.timeout and time.time() - start_time > self.timeout - 10:
                    self.logger.warning("Rucio took longer than %ds to respond, trying again (attempt %d)" % (
                        self.timeout - 10, attempt + 1))
                    result = None
                    continue
                break
        return result


class RPG:
    """
    Main class processing subscriptions
    """

    def __init__(self, configfile):
        self.configfile = configfile

        # CA certificate for Rucio and CRIC
        self.ca_cert = '/etc/ssl/certs/CERN-bundle.pem' if os.path.exists('/etc/ssl/certs/CERN-bundle.pem') else False
        # moved rucio initialisation to __getattr__ (lazy initialisation)

        # make this logger the root logger, taking over from Rucio's logger
        self.logger = logging.getLogger()
        self.logger.handlers = []

        # Information on current status
        # Destination share (eg free space): {site: share}
        self.destsiteshare = {}
        # Datasets on source sites {site: [datasets]}
        self.srcdatasets = {}
        # Datasets to add to containers
        self.containerdatasets = []
        # Current subscriptions to destination sites {site: [datasets]}
        self.subscriptions = {}
        # Maximum subscription limit per site {site: limit}
        self.sitemaxsubs = {}
        # Datasets and replicas and subscriptions {dataset: [sites]}
        self.datasetnum = defaultdict(list)
        # Counts of non-local subscriptions per site for logging purposes {site: num}
        self.nonlocalsubcount = defaultdict(int)
        # Map of src site to dest site for sites using other site as 'local' dest
        self.sitemap = {}
        # Dataset patterns to match
        self.datasetpatterns = []
        # Text for email notification if configured
        self.emailbody = ''

    def __getattr__(self, attr_name):
        # lazy init of rucio,
        # moved from constructor to be able to use configuration parameters (cache_dir_common, rucio_host)
        if attr_name == 'rucio':  # if rucio is not initialized yet
            # use of dump (dump_force) can be disabled by Debug.dump_force=None
            if 'dump_force' not in Debug.__dict__ or Debug.dump_force is not None:
                self.dump_force = DumpForce(cache_dir_common=self.cache_dir_common)
            self.rucio = Wrapper(Client(ca_cert=self.ca_cert, rucio_host='http://rucio-lb-prod.cern.ch:80'),
                                 'RucioWrapper', 600, dump_force=self.dump_force)
            return self.rucio  # self.__dict__[attr_name]
        raise AttributeError(attr_name)

    @log_time
    def parseConf(self):
        """
        Parse configuration and set up logging, raises exception if any error occurred.
        """
        config = cp.ConfigParserExtended()
        if not config.read_extended(self.configfile):
            raise RPGException("Cannot read configuration from " + self.configfile)

        try:
            # set up logging based on configuration
            # use log_dir as prefix to log_file
            log_dir = '' if not config.has_option("log", "log_dir") else config.get("log", "log_dir").rstrip('/')
            log_file = log_dir + '/' + config.get("log", "log_file")
            if not os.path.exists(log_file[:log_file.rfind('/')]):
                os.makedirs(log_file[:log_file.rfind('/')], 0o755)
            self.log_level = Debug.log_level if 'log_level' in Debug.__dict__ else config.get("log", "log_level")
            fmt = logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s")
            handler = logging.FileHandler(log_file)
            handler.setFormatter(fmt)
            self.logger.setLevel(getattr(logging, self.log_level.upper()))
            self.logger.addHandler(handler)
            self.logger.propagate = False  # to stop also sending to stderr
            self.logger.info('set log level: %s', self.log_level.upper())  # warn?
        except Exception as e:
            self.logger.error("Failed to set up logging: %s - will log to stderr at WARNING level", str(e))
            self.logger.setLevel(logging.WARN)

        try:
            self.logger.info("Parsing configuration at %s", self.configfile)
            # Take conf file without suffix as cache sub dir
            cache_dir_base = '' if not config.has_option("cache", "cache_dir_base") \
                else config.get("cache", "cache_dir_base").rstrip('/')
            self.cachedir = cache_dir_base + '/' + config.get("cache", "cache_dir_sub") + \
                '/' + os.path.basename(self.configfile).split('.')[0]
            # Common cache dir to store common inter-rpg-run data
            self.cache_dir_common = cache_dir_base + '/' + config.get("cache", "cache_dir_common")
            # Datasets already done
            self.donedatasets = CachedDataHandler("/".join([self.cachedir, "done"]))
            # Datasets too old
            self.tooolddatasets = CachedDataHandler("/".join([self.cachedir, "tooold"]))
            # Datasets too large
            self.toolargedatasets = CachedDataHandler("/".join([self.cachedir, "toolarge"]))
            # Datasets too small
            self.toosmalldatasets = CachedDataHandler("/".join([self.cachedir, "toosmall"]))
            # Datasets to copy (so don't check metadata)
            self.tododatasets = CachedDataHandler("/".join([self.cachedir, "todo"]))
            if 'test' in Debug.__dict__:
                self.test = Debug.test
            else:
                self.test = config.has_section("test")
            # Mandatory parameters
            self.destsites = config.get("sites", "destination_sites").split()
            self.srcsites = config.get("sites", "source_sites").split()
            self.destspacetoken = config.get("sites", "dest_token")
            self.srcspacetoken = config.get("sites", "source_token")
            self.numreplicas = config.getint("policy", "replicas")

            # Optional parameters
            self.blacklistedsrc = [] if not config.has_option("sites", "sites_blacklisted_src") else config.get("sites",
                                                                                                                "sites_blacklisted_src").split()
            self.blacklisteddest = [] if not config.has_option("sites", "sites_blacklisted_dest") else config.get(
                "sites", "sites_blacklisted_dest").split()
            sitemapping = [] if not config.has_option("sites", "site_map") else config.get("sites", "site_map").split(
                ',')
            if sitemapping and sitemapping[0]: self.sitemap = dict(
                (site.split()[0], site.split()[1]) for site in sitemapping)

            self.siteweight = 'random' if not config.has_option("policy", "site_weight") else config.get("policy",
                                                                                                         "site_weight")
            self.preferlocal = False if not config.getboolean("policy", "prefer_local") else config.getboolean("policy",
                                                                                                               "prefer_local")
            self.completesrc = True if not config.getboolean("policy", "complete_src") else config.getboolean("policy",
                                                                                                              "complete_src")
            self.explicitsrc = True if not config.has_option("policy", "explicit_src") else config.getboolean("policy",
                                                                                                              "explicit_src")
            self.skiptransient = True if not config.has_option("policy", "skip_transient") else config.getboolean(
                "policy", "skip_transient")
            self.destlifetime = None if not config.has_option("policy", "destination_lifetime") else config.getfloat(
                "policy", "destination_lifetime")
            if self.destlifetime:
                self.destlifetime *= 86400  # Rucio rules have lifetime in seconds
            self.destlocked = False if not config.getboolean("policy", "destination_locked") else config.getboolean(
                "policy", "destination_locked")
            self.sourcestate = None if not config.has_option("policy", "source_state") else config.get("policy",
                                                                                                       "source_state")
            self.activity = 'Data Consolidation' if not config.has_option("policy", "activity") else config.get(
                "policy", "activity")
            self.account = None if not config.has_option("policy", "account") else config.get("policy", "account")
            self.comment = 'Created by RPG' if not config.has_option("policy", "comment") else config.get("policy",
                                                                                                          "comment")
            self.asynchronous = False if not config.has_option("policy", "asynchronous_rule") else config.getboolean(
                "policy", "asynchronous_rule")

            if 'email' in Debug.__dict__:
                self.email = Debug.email
            else:
                self.email = config.get("policy", "email").split() if config.has_option("policy", "email") else None

            self.almostdone = True if not config.has_option("policy", "almost_done") else config.getboolean("policy",
                                                                                                            "almost_done")

            self.maxsubs = 100000 if not config.getint("limits", "max_subscriptions") else config.getint("limits",
                                                                                                         "max_subscriptions")
            self.max_dataset_length = None if not config.has_option("limits", "max_dataset_length") else config.getint(
                "limits", "max_dataset_length")
            self.minfrozen = None if not config.has_option("limits", "min_frozen_time") else timedelta(
                config.getfloat("limits", "min_frozen_time"))
            self.maxage = None if not config.has_option("limits", "max_age") else timedelta(
                config.getfloat("limits", "max_age"))
            self.minsize = None if not config.has_option("limits", "min_size") else config.getint("limits", "min_size")
            self.maxsize = None if not config.has_option("limits", "max_size") else config.getint("limits", "max_size")
            self.containerlimit = -1 if not config.has_option("limits", "container_limit") else config.getint("limits",
                                                                                                              "container_limit")
            self.containerminsize = None if not config.has_option("limits", "container_min_size") else config.getint(
                "limits", "container_min_size")
            self.containerminlength = None if not config.has_option("limits",
                                                                    "container_min_length") else config.getint("limits",
                                                                                                               "container_min_length")

            self.metadata = config.items("metadata") if config.has_section("metadata") else []

            if config.has_option("datasets", "projects") and config.has_option("datasets",
                                                                               "types") and config.has_option(
                "datasets", "tags"):
                projects = config.get("datasets", "projects").split()
                types = config.get("datasets", "types").split()
                tags = config.get("datasets", "tags").split()
                self.datasetpatterns = ['.*'.join(p) + '.*' for p in itertools.product(projects, types, tags)]
            if config.has_option("datasets", "patterns"):
                self.datasetpatterns.extend(config.get("datasets", "patterns").split())
            if not self.datasetpatterns:
                raise RPGException("No dataset patterns defined in configuration")
            self.scopes = [] if not config.has_option("datasets", "scopes") else config.get("datasets",
                                                                                            "scopes").split()
            self.exclude = [] if not config.has_option("datasets", "exclude") else config.get("datasets",
                                                                                              "exclude").split()
            self.containerpattern = None if not config.has_option("datasets", "container_pattern") else config.get(
                "datasets", "container_pattern")
        except configparser.Error as e:
            raise RPGException("Configuration error: " + str(e))

    @log_time
    def getCRICInfo(self):
        """
        Get site information and blacklisting information from CRICHandler
        """
        self.logger.info("Gathering info from CRIC")
        certificate = os.environ.get('X509_USER_PROXY', '/tmp/x509up_u{}'.format(os.getuid()))
        if not os.path.exists(certificate):
            raise RPGException("No certificate found at {}".format(certificate))
        try:
            # Get DDM endpoint info, expand T1, T2 etc and remove disabled sites
            CRICHandler.getDDMEndpoints(self.cachedir, self.srcsites, self.destsites, self.srcspacetoken,
                                        self.destspacetoken, certificate)
            (blacklistedsources, blacklisteddests) = CRICHandler.getBlacklistedEndpoints(self.cachedir, certificate)
        except CRICHandler.CRICException as e:
            raise RPGException("Failed to retrieve CRIC information: %s" % str(e))

        if len(self.destsites) < self.numreplicas:
            raise RPGException(
                "Cannot have more replicas (%d) than destination sites (%d)!" % (self.numreplicas, len(self.destsites)))

        for source in blacklistedsources:
            try:
                site = c.list_rse_attributes(source)['site']
            except:
                continue  # probably missing in rucio
            site = source[:source.find('_')]
            if site in self.srcsites and re.search('_%s$' % self.srcspacetoken, source):
                self.blacklistedsrc.append(site)
        for dest in blacklisteddests:
            try:
                site = c.list_rse_attributes(dest)['site']
            except:
                continue  # probably missing in rucio
            site = dest[:dest.find('_')]
            if site in self.destsites and re.search('_%s$' % self.destspacetoken, dest):
                self.blacklisteddest.append(site)

        # Remove manually blacklisted sites if not in site list
        self.blacklistedsrc = [s for s in self.blacklistedsrc if s in self.srcsites]
        self.blacklisteddest = [s for s in self.blacklisteddest if s in self.destsites]
        self.logger.info('Blacklisted source sites: %s', self.blacklistedsrc)
        self.logger.info('Blacklisted destination sites: %s', self.blacklisteddest)

        # Reduce numreplicas if not possible to make them all
        self.numreplicas = min(self.numreplicas, len(self.destsites) - len(self.blacklisteddest))
        self.logger.info('Number of replicas to create: %d' % self.numreplicas)
        if not self.numreplicas:
            self.logger.warning('All destinations are blacklisted, exiting')
            sys.exit(0)

    @log_time
    def getCurrentData(self):
        """
        Loop over destination sites and get current rules.
        Loop over source sites and get datasets to replicate.
        """
        self.logger.info("Replication starting")
        for site in self.destsites:
            destsite = site + "_" + self.destspacetoken
            # List rules
            # TODO if it becomes possible to list only replicating rules then we can 
            # go back to using the cache rather than getting all datasets on site
            rules = self.rucio.get_dataset_locks_by_rse(destsite)
            if rules is None:
                raise RPGException("Could not find rules in " + destsite)

            # TODO agb: optimize up to one pass
            # filter out datasets to ignore
            rules = [r for r in rules if not [p for p in self.exclude if re.search(p, r['name'])]]
            self.subscriptions[site] = []

            # Ignore SUSPENDED rules and rules with less than 10 files stuck/replicating
            # unless the almostdone option is set to False
            for rule in [ds for ds in rules if ds['state'] in ['REPLICATING', 'STUCK']]:
                if not self.almostdone:
                    self.subscriptions[site].append((rule['scope'], rule['name']))
                    continue
                # if we get locks from the dump, we have all locks counter info in the rule already
                if 'locks_replicating_cnt' in rule.keys():
                    res = (rule['locks_replicating_cnt'] + rule['locks_stuck_cnt'] > 10)
                else:
                    rule_state = self.rucio.get_replication_rule(rule['rule_id'])
                    res = rule_state and rule_state['state'] != 'SUSPENDED' and \
                          (rule_state.get('locks_replicating_cnt', 0) + rule_state.get('locks_stuck_cnt', 0) > 10)
                if res:
                    self.subscriptions[site].append((rule['scope'], rule['name']))
                else:
                    self.logger.debug('Ignoring %s' % rule['name'])
            # self.logger.debug('Replicating rules: %s', self.subscriptions[site])
            self.logger.info("%i replicating rules (excluding tmp datasets) to %s", len(self.subscriptions[site]),
                             destsite)
            rules = [(ds['scope'], ds['name']) for ds in rules if ds['state'] in ['OK', 'REPLICATING', 'STUCK']]

            # Loop over the product of projects, types and tags
            for datasetpattern in self.datasetpatterns:
                for ds in rules:
                    if re.match(datasetpattern, ds[1]) and (not self.scopes or ds[0] in self.scopes) and site not in \
                            self.datasetnum[ds]:
                        self.datasetnum[ds].append(site)

        for site in self.srcsites:
            if site in self.blacklistedsrc:
                self.logger.info("%s is currently blacklisted for reading, skip looking up datasets", site)
                continue

            self.srcdatasets[site] = []
            srcsite = site + "_" + self.srcspacetoken

            # Get all datasets in site then check for the patterns we need
            rules = self.rucio.get_dataset_locks_by_rse(srcsite)
            if rules is None:
                raise RPGException("Failed to query datasets in " + srcsite)

            # todo: agb: profile, optimize
            if self.completesrc:
                rules = [r for r in rules if r['state'] == 'OK']
            else:
                rules = [r for r in rules if r['state'] in ['OK', 'REPLICATING', 'STUCK']]

            # filter out datasets to ignore
            rules = [r for r in rules if not [p for p in self.exclude if re.search(p, r['name'])]]

            # Apply source policy before filtering out datasets with enough copies
            self._handleSources([(r['scope'], r['name']) for r in rules if (r['scope'], r['name']) in self.datasetnum],
                                srcsite)

            # filter out datasets with enough copies done or replicating
            rules = [r for r in rules if not ((r['scope'], r['name']) in self.datasetnum and len(
                self.datasetnum[(r['scope'], r['name'])]) >= self.numreplicas)]

            # Remove datasets already done but not seen (stuck in inject)
            rules = [r for r in rules if self.numreplicas > 1 or not self.donedatasets.hasEntry(r['name'])]

            # Loop over the product of projects, types and tags
            for datasetpattern in self.datasetpatterns:
                self.srcdatasets[site].extend([(r['scope'], r['name']) for r in rules if
                                               re.match(datasetpattern, r['name']) and (
                                                       not self.scopes or r['scope'] in self.scopes)])

            # Filter out duplicates (rules for same dataset with different owners)
            self.srcdatasets[site] = list(set(self.srcdatasets[site]))
            self.logger.info("%i matching datasets in %s", len(self.srcdatasets[site]), site)

    @log_time
    def getDestinationWeights(self):
        """
        Get the weights to use for each destination depending on what is
        configured, either pledge free space or ABCD metric.
        """

        if self.siteweight == 'pledge_free':
            self._getPledges()
        elif self.siteweight == 'abcd':
            self._getABCD()
        elif self.siteweight == 'mou':
            self._getMoU()
        elif self.siteweight == 'random':
            self.destsiteshare = dict((site, 1.0) for site in self.destsites)
        else:
            raise RPGException("Bad destination_weight setting: " + self.siteweight)

        # Set zero share for sites with no weight info or blacklisted for writing
        for site in self.destsites:
            if site in self.blacklisteddest:
                self.logger.info("%s is blacklisted for writing, setting zero share" % site)
                self.destsiteshare[site] = 0
            elif site not in self.destsiteshare:
                self.logger.info("No weight info for %s, setting zero share", site)
                self.destsiteshare[site] = 0

    @log_time
    def filterData(self):
        """
        Filter out datasets according to metadata
        """
        currenttime = datetime.utcnow()  # Rucio timestamps are UTC
        for site in self.srcdatasets:
            for ds in self.srcdatasets[site][:]:

                # Check if we need to look up any metadata
                if not (self.minfrozen or self.metadata or self.skiptransient or self.maxage or self.minsize) or \
                        (not self.destlifetime and self.tododatasets.hasEntry(ds[1])):
                    continue

                # Check cached data first
                if (self.maxage is not None and self.tooolddatasets.hasEntry(ds[1])) or \
                        (self.minsize is not None and self.toosmalldatasets.hasEntry(ds[1])) or \
                        (self.maxsize is not None and self.toolargedatasets.hasEntry(ds[1])):
                    self.srcdatasets[site].remove(ds)
                    continue

                try:
                    ruciomd = self.rucio.get_metadata(ds[0], ds[1])
                except DataIdentifierNotFound:
                    self.logger.warning("Dataset %s:%s not found", ds[0], ds[1])
                    self.srcdatasets[site].remove(ds)
                    continue

                if not ruciomd:
                    self.srcdatasets[site].remove(ds)
                    continue

                # Filter by freezing date and other configured metadata values
                if self.minfrozen is not None:
                    if not ruciomd['closed_at'] or ruciomd['is_open']:
                        self.srcdatasets[site].remove(ds)
                        self.logger.info("%s removed from list because it is still open", ds[1])
                        continue
                    if ruciomd['closed_at'] + self.minfrozen > currenttime:
                        self.srcdatasets[site].remove(ds)
                        self.logger.info("%s removed from list because it was closed less than %d days ago", ds[1],
                                         self.minfrozen.days)
                        continue
                if self.maxage is not None:
                    if not ruciomd['created_at']:
                        self.logger.warning("%s: Cannot determine the created_at of %s - skipping the dataset", site,
                                            ds[1])
                        self.srcdatasets[site].remove(ds)
                        continue
                    if ruciomd['created_at'] + self.maxage < currenttime:
                        self.logger.info("%s: %s removed from list because it is too old. Created_at: %s", site, ds[1],
                                         str(ruciomd['created_at']))
                        self.srcdatasets[site].remove(ds)
                        self.tooolddatasets.addEntry(ds[1])
                        continue
                if self.skiptransient:
                    if 'transient' in ruciomd and ruciomd['transient'] in [1, True]:
                        self.logger.info("%s: skipping transient dataset", ds[1])
                        self.srcdatasets[site].remove(ds)
                        continue
                if self.minsize is not None:
                    if not ruciomd['length'] or not ruciomd['bytes']:
                        self.logger.warning("%s: Cannot obtain dataset size for %s" % (site, ds[1]))
                    elif ruciomd['bytes'] / ruciomd['length'] < self.minsize:
                        self.logger.warning("%s: %s removed from list because average file size (%i) is too small" % (
                            site, ds[1], ruciomd['bytes'] / ruciomd['length']))
                        self.srcdatasets[site].remove(ds)
                        if self.containerpattern:
                            if (self.containerminsize and ruciomd['bytes'] < self.containerminsize) or \
                                    (self.containerminlength and ruciomd['length'] < self.containerminlength):
                                self.logger.info("%s: %s too small (%d bytes, %d files) to add to container" % (
                                    site, ds[1], ruciomd['bytes'], ruciomd['length']))
                                self.toosmalldatasets.addEntry(ds[1])
                            else:
                                self.logger.info("%s: Will add %s to container" % (site, ds[1]))
                                # Store all metadata since any attribute can be substituted in the container name
                                self.containerdatasets.append(ruciomd)
                        else:
                            self.toosmalldatasets.addEntry(ds[1])
                        continue
                if self.maxsize is not None:
                    if not ruciomd['length'] or not ruciomd['bytes']:
                        self.logger.warning("%s: Cannot obtain dataset size for %s" % (site, ds[1]))
                    elif ruciomd['bytes'] / ruciomd['length'] > self.maxsize:
                        self.logger.warning("%s: %s removed from list because average file size (%i) is too large" % (
                            site, ds[1], ruciomd['bytes'] / ruciomd['length']))
                        self.srcdatasets[site].remove(ds)
                        self.toolargedatasets.addEntry(ds[1])
                        continue
                metamatch = True
                for (key, value) in self.metadata:
                    if key not in ruciomd or str(ruciomd[key]) not in value.split():
                        self.logger.info(
                            "%s: %s removed from the migration list due to a metadata mismatch: %s is not in [%s]",
                            site, ds[1], key, value)
                        self.srcdatasets[site].remove(ds)
                        metamatch = False
                        break
                if not metamatch:
                    continue

                self.tododatasets.addEntry(ds[1])
            self.logger.info("%s: %i datasets to be replicated", site, len(self.srcdatasets[site]))

    @log_time
    def assignSites(self):
        """
        Take the lists of candidate datasets and assign them to dest sites
        """
        # Firstly loop over sites and calculate their share of new subscriptions
        self._calculateShares()

        # Secondly loop over datasets and assign to local site if desired up to pledge fraction
        self._assignToLocal()

        # Thirdly loop over datasets randomly and assign until subscription limits
        # are reached or all datasets are done.
        self._assignRandomly()

        # Finally, if prefer local is true, take sites which have not had any
        # datasets subscribed and assign one to each destination site (to clear backlog)
        # DC 23.5.17: disable this to avoid overloading sites
        # self._assignBacklog()

        for site in self.destsites:
            self.logger.info("%i subscriptions made to %s from other sites", self.nonlocalsubcount[site], site)
        self.logger.info("Finished subscribing datasets")

        # Assign small datasets to container for zipping
        self._assignToContainer()

    @log_time
    def send_email(self, exception_str=None, config=None):
        """
        If configured send email report of rules made.
        """
        if exception_str is not None:
            self.emailbody = "exception happened: %s" % exception_str
        else:
            # send mail report when Debug.email_empty flag is True even if no new rules were added
            if not self.emailbody:
                if 'email_empty' in Debug.__dict__ and Debug.email_empty is True:
                    self.emailbody = "<no rules were created>"
        try:
            if self.email and self.emailbody:
                self.logger.info("Compose email report to %s" % self.email)
                msg = MIMEMultipart("alternative")  # "alternative" for text+html only. With attachments use "mixed"
                # try to set real user/host into from_email
                message_top: str = ""
                from_email = "ddmusr01@cern.ch" # default @todo fixme
                try:
                    from_email = "%s@cern.ch" % (getuser())
                    message_top += "RPG report generated by %s@%s (%s):\n" % \
                                   (getuser(), getfqdn(), datetime.now().strftime('%Y-%m-%d %H:%M:%S %%s') % utc_diff())
                except Exception as ex:
                    message_top += "// sender address %s may be incorrect due to exception: %s\n" \
                                   % (from_email, str(ex))
                msg['From'] = from_email
                msg['To'] = ", ".join(self.email)  # recipients as string
                msg['Subject'] = "RPG " + ("automatic notification" if exception_str is None else "failure") + \
                                 (': ' + config if config is not None else '')
                backlog = len([ds for s in self.srcdatasets for ds in self.srcdatasets[s]])
                text = message_top + (self.emailbody if exception_str is not None
                                      else "%s generated the following rules - %i datasets still to be replicated\n\nDATASET SOURCE DESTINATION\n%s" %
                                           ("DRY-RUN mode!\nRPG would have" if self.test else "RPG", backlog, self.emailbody))
                part1 = MIMEText(text, "plain")
                # todo: to please users with nice html-letters the next code may be adopted:
                # html = "nice html report here<>"
                # part2 = MIMEText(html, "html")

                msg.attach(part1)
                s = smtplib.SMTP('localhost')
                s.sendmail(from_email, self.email, msg.as_string())  # recipients as list
                s.quit()
                self.logger.info("%s report was sent to recipients '%s'" %
                                 ("Successful" if exception_str is None else "Failure", self.email))
            else:
                s = ("e-mail address is not defined" if not self.email else "")
                s = s + ((" and " if s else "") + "e-mail body is not defined") if not self.emailbody else ""
                self.logger.info("Can not compose email to %s since %s" % (self.email, s))
        except Exception as ex:
            raise RPGException("Exception in sendMail(): %s" % str(ex))

    #  @log_time commented: 0sec
    def _handleSources(self, datasets, site):
        """
        Apply policy for source replicas where transfer has completed.
        """
        if not self.sourcestate or (self.sourcestate != 'delete' and self.sourcestate != 'secondary'):
            return

        purge_replicas = True if self.sourcestate == 'delete' else None

        # Loop over datasets and check if there are enough OK replicas
        for dataset in datasets:
            rules = list(self.rucio.get_dataset_locks(dataset[0], dataset[1]))
            goodrules = [r for r in rules if r['state'] == 'OK' and r['rse'].rsplit('_', 1)[0] in self.destsites and
                         r['rse'].rsplit('_', 1)[1] == self.destspacetoken]
            # Remove replicas also in source sites otherwise we delete replicas
            # we want to keep
            goodrules = [r for r in goodrules if not (
                    r['rse'].rsplit('_', 1)[0] in self.srcsites and r['rse'].rsplit('_', 1)[
                1] == self.srcspacetoken)]
            self.logger.debug(
                "%s:%s has complete rules on %s" % (dataset[0], dataset[1], [r['rse'] for r in goodrules]))
            if len(goodrules) >= self.numreplicas:
                # Get the source rule id and delete it
                srcrule = [r for r in rules if r['rse'] == site]
                if not srcrule:
                    self.logger.warning("Source rule for %s:%s was unexpectedly deleted" % (dataset[0], dataset[1]))
                else:
                    ruleid = srcrule[0]['rule_id']
                    self.logger.info(
                        "Deleting rule %s for %s:%s on %s" % (ruleid, dataset[0], dataset[1], srcrule[0]['rse']))
                    if self.test:
                        self.logger.warning("No real deletion done")
                    else:
                        self.rucio.delete_replication_rule(ruleid, purge_replicas=purge_replicas)

    @log_time
    def _calculateShares(self):
        """
        Returns a dictionary of site: max subscriptions based on site weight
        """
        totalweight = sum([share for share in self.destsiteshare.values() if share > 0])
        self.logger.info("Total of weights: %f", totalweight)
        if not totalweight:
            raise RPGException("No weight information available")

        # Firstly loop over sites and calculate their share of new subscriptions
        for site in self.destsites:
            # Skip sites with no share info
            if site not in self.destsiteshare:
                self.logger.error("No share info for %s, skipping", site)
                self.sitemaxsubs[site] = 0
                continue
            siteshare = float(self.destsiteshare[site]) / float(totalweight)
            if siteshare < 0:
                siteshare = 0
            # Spread all subscriptions slots according to share. For tape
            # this assumes that tape disk buffer size scales with pledge and %
            # free space is roughly the same across sites. It also assumes that
            # all subscriptions are processed by the time of the next run. Casting
            # to int means sites with less than 1/maxsubs share won't get any.
            self.sitemaxsubs[site] = int(siteshare * self.maxsubs)
            # If copying to disk the max subs does not include current subscriptions
            if not self.destspacetoken.endswith('TAPE'):
                self.sitemaxsubs[site] += len(self.subscriptions[site]) + 1
            self.logger.info("%s: share %f, max subscriptions %i", site, siteshare, self.sitemaxsubs[site])

    @log_time
    def _assignToLocal(self):
        """
        Assign subscriptions to local site if desired
        """
        if not self.preferlocal or self.srcspacetoken == self.destspacetoken:
            return
        for site in self.srcdatasets:
            # Check configured mapping of local sites 
            destsite = site if site not in self.sitemap else self.sitemap[site]
            # Check if the dest site is a configured destination
            if destsite not in self.destsites:
                continue
            # Subscribe until limit of pledge or subscriptions is reached
            datasetsleft = len(self.srcdatasets[site])
            for ds in self.srcdatasets[site][:]:
                if len(self.subscriptions[destsite]) >= self.sitemaxsubs[destsite]:
                    break
                # Check if we have reached the limit of replicas with previous
                # subscriptions of this dataset
                if len(self.datasetnum[ds]) >= self.numreplicas:
                    self.srcdatasets[site].remove(ds)
                    continue

                if self._registerSubscription(ds, destsite, site) is None:
                    # Retry to another site in the next step
                    continue

                # Check limit again
                if len(self.datasetnum[ds]) >= self.numreplicas:
                    self.srcdatasets[site].remove(ds)

            self.logger.info("%i subscriptions made to %s from %s, %i datasets left",
                             datasetsleft - len(self.srcdatasets[site]), destsite, site, len(self.srcdatasets[site]))

    @log_time
    def _assignRandomly(self):
        """
        Assign random datasets to random destinations until a limit is reached
        """
        while self.srcdatasets:
            srcsite = random.choice(list(self.srcdatasets.keys()))
            if not self.srcdatasets[srcsite]:
                self.srcdatasets.pop(srcsite)
                continue
            dataset = random.choice(self.srcdatasets[srcsite])
            # Check if limit is reached for this dataset
            if len(self.datasetnum[dataset]) >= self.numreplicas:
                self.srcdatasets[srcsite].remove(dataset)
                continue

            destsites = self.subscriptions.keys()
            # Make sure not to subscribe to existing replicas
            destsites = [site for site in destsites if site not in self.datasetnum[dataset]]

            # Remove local destination if preferlocal is false
            if not self.preferlocal and srcsite in destsites:
                destsites.remove(srcsite)

            # Shuffle destination sites based on the share
            destsites = self._weightedRandomShuffle(destsites)
            sitefound = False
            for destsite in destsites:
                if len(self.subscriptions[destsite]) < self.sitemaxsubs[destsite] \
                        and dataset not in self.subscriptions[destsite]:
                    # If this fails ignore and next run a different site should be chosen
                    self._registerSubscription(dataset, destsite, srcsite)
                    # Check if limit is reached for this dataset
                    if len(self.datasetnum[dataset]) >= self.numreplicas:
                        self.srcdatasets[srcsite].remove(dataset)
                    sitefound = True
                    self.nonlocalsubcount[destsite] += 1
                    break

            if not sitefound:
                # No site had any slots left, we are done
                self.logger.info("Subscription limit reached at all sites. %i datasets could not be fully replicated",
                                 len([ds for s in self.srcdatasets for ds in self.srcdatasets[s]]))
                break

    @log_time
    def _assignBacklog(self):
        """
        If local replication is preferred, take sites with no possibility to
        replicate locally and assign one dataset to each dest site
        """
        if not self.preferlocal:
            return
        for site in self.srcdatasets:
            if site not in self.sitemaxsubs or self.sitemaxsubs[site] == 0:
                # Don't use sites with no share
                destsites = [key for key in self.destsiteshare.keys() if self.sitemaxsubs[key] > 0]
                random.shuffle(destsites)
                for destsite in destsites:
                    if len(self.srcdatasets[site]) == 0:
                        break
                    ds = self.srcdatasets[site].pop()
                    if len(self.datasetnum[ds]) < self.numreplicas:
                        # If this fails ignore and next run a different site should be chosen
                        self._registerSubscription(ds, destsite, site)
                        self.nonlocalsubcount[destsite] += 1

    @log_time
    def _assignToContainer(self):
        """
        Take datasets that are below the size limit and add them to containers
        specified by containerpattern
        """

        if not (self.containerdatasets and self.containerpattern and self.containerlimit):
            return

        # Keep a track of datasets already in containers
        containercontents = {}

        for ds in self.containerdatasets:
            if self.containerlimit == 0:
                self.logger.info("Reached limit of datasets to add to containers")
                break
            try:
                cscope, cname = self.containerpattern.format(**ds).split(':')
            except KeyError as e:
                self.logger.error("%s: could not substitute metadata in container pattern: %s" % (ds['name'], str(e)))
                continue
            if cname not in containercontents:
                try:
                    content = [(c['scope'], c['name']) for c in self.rucio.list_content(cscope, cname)]
                except DataIdentifierNotFound:
                    self.logger.error("No such container %s:%s" % (cscope, cname))
                    continue
                containercontents[cname] = content

            # Check if the dataset is already there
            if (ds['scope'], ds['name']) in containercontents[cname]:
                self.logger.debug("%s already in %s:%s" % (ds['name'], cscope, cname))
                continue

            self.logger.info("Adding %s to %s:%s" % (ds['name'], cscope, cname))
            if self.test:
                self.logger.info("Dry-run mode, no real operations")
            else:
                self.rucio.add_datasets_to_container(cscope, cname, [{'scope': ds['scope'], 'name': ds['name']}])
            self.containerlimit -= 1

    def _getPledges(self):
        """
        Look up CRIC and get disk or tape pledges for ATLAS sites. Keep a
        cached copy in case service is down. The numbers should anyway not change
        much over time. Also look up used tape space from Rucio.
        """
        # Mapping between WLCG site name ("Federation") and ATLAS name
        sitemap = {"CA-TRIUMF": "TRIUMF-LCG2", "FR-CCIN2P3": "IN2P3-CC",
                   "DE-KIT": "FZK-LCG2", "IT-INFN-CNAF": "INFN-T1",
                   "NL-T1": "SARA-MATRIX", "NDGF": "NDGF-T1",
                   "ES-PIC": "PIC", "TW-ASGC": "TAIWAN-LCG2",
                   "UK-T1-RAL": "RAL-LCG2", "US-T1-BNL": "BNL-OSG2",
                   "NRC-KI-T1": "RRC-KI-T1", "CH-CERN": "CERN-PROD"}
        url = "https://wlcg-cric.cern.ch/api/core/federation/query/?json&tier_level=1&vo=atlas&preset=current"
        cache = "/".join([self.cachedir, "cric.data"])
        try:
            data = requests.get(url, verify=self.ca_cert).json()
        except Exception as e:
            self.logger.warning("Failed to get pledge info from %s: %s", url, str(e))
            try:
                with open(cache) as data_file:
                    data = json.load(data_file)
                self.logger.info("Loaded pledge info from cache")
            except Exception as e:
                raise RPGException("Failed to get pledge information from CRIC or cache")
        else:
            # dump info to cache
            with open(cache, 'w') as data_file:
                json.dump(data, data_file)

        # do we want disk or tape pledge?
        pledgetype = 'Disk' if 'DISK' in self.destspacetoken else 'Tape'
        try:
            for federation, pledge_info in data.items():
                if federation not in sitemap:  # Ignore non-ATLAS sites
                    continue
                site = sitemap[federation]
                if site not in self.destsites:  # Don't count sites we are not using
                    continue
                pledged = pledge_info['pledges']['atlas'][pledgetype]
                # Artificially bump up all pledges to write over pledge
                pledged = pledged * 1.2
                # Get used space from Rucio
                used = self._getUsedSpace(site, pledgetype)
                self.logger.debug("Site %s: %s pledge %d TB, used %d TB", site, pledgetype, pledged, used)
                self.destsiteshare[site] = pledged - used
        except KeyError as e:
            raise RPGException("Badly formatted pledge data from CRIC: " + str(e))

    def _getABCD(self):
        """
        Get ABCD metric data and apply to relevant destination sites.
        """
        # Real data not implemented yet
        for site in self.destsites:
            self.destsiteshare[site] = 1.0

    def _getMoU(self):
        """
        Get the MoU share for each destination site. Hard-coded for T1 now but
        should eventually come from CRIC
        """
        mou = {'BNL-OSG2': 20, 'FZK-LCG2': 11, 'IN2P3-CC': 11, 'INFN-T1': 9,
               'NDGF-T1': 5, 'PIC': 5, 'RAL-LCG2': 11, 'SARA-MATRIX': 12,
               'TAIWAN-LCG2': 7, 'TRIUMF-LCG2': 9}

        self.destsiteshare = mou

    def _getUsedSpace(self, site, spacetype):
        """
        Call Rucio to get the used space by querying Rucio. For tape use the
        Rucio-reported space because SRM usually only reports the disk buffer.
        In this case all tape endpoints (DATATAPE, MCTAPE, GROUPTAPE etc) have
        to be counted. For disk use the SRM-reported used space, which assumes
        all pledged space is in the dest space token (probably close enough for
        T1 DATADISK). UsedTB returned is TB and not TiB since pledges are in TB.
        """
        usedTB = 0.
        if spacetype == 'Tape':
            endpoints = [e['rse'] for e in self.rucio.list_rses() if
                         e['rse_type'] == 'TAPE' and re.match('%s_' % site, e['rse'])]

            for tapesite in endpoints:
                tapespace = self.rucio.get_rse_usage(tapesite, filters={'source': 'rucio'})

                if not tapespace:
                    # Group tape is sometimes empty, log warning instead of fatal error
                    # raise RPGException("Could not find used space in "+tapesite)
                    self.logger.warning("Could not find used space in " + tapesite)
                    continue
                try:
                    # TB since pledges are in TB
                    used = tapespace[0]['used'] / 1000 / 1000 / 1000 / 1000
                except:
                    raise RPGException("Could not find used space in " + tapesite)

                usedTB += used
                self.logger.info("%s: used space %f", tapesite, used)
            self.logger.info("%s: total used tape space %f", site, usedTB)

        else:
            disksite = site + "_" + self.destspacetoken
            diskspace = self.rucio.get_rse_usage(disksite, filters={'source': 'srm'})

            try:
                usedTB += diskspace[0]['used'] / 1000 / 1000 / 1000 / 1000
            except:
                raise RPGException("Could not find used space in " + disksite)

            self.logger.info("%s: used space %f", disksite, usedTB)

        return usedTB

    def _weightedRandomShuffle(self, destsites):
        """
        Shuffle destsites, but based on the weighting in destsiteshare, so that
        sites with large weights are more likely to come earlier in the list.
        """
        # weightlist contains each site repeated weight number of times
        weightlist = []
        # shuffledlist contains the shuffled sites
        shuffledlist = []
        for site in destsites:
            weightlist.extend([site for i in range(int(self.destsiteshare[site]))])

        while weightlist:
            site = weightlist[random.randint(0, len(weightlist) - 1)]
            shuffledlist.append(site)
            weightlist = [s for s in weightlist if s != site]

        return shuffledlist

    def _registerSubscription(self, dsn, site, source=None):
        """
        Call Rucio to add a new rule
        """
        dids = [{'scope': dsn[0], 'name': dsn[1]}]
        destsite = site + '_' + self.destspacetoken
        if not self.explicitsrc:
            source = None
        if source:
            source += '_' + self.srcspacetoken

        self.logger.debug("Adding rule to %s from %s for %s:%s", destsite, source, dsn[0], dsn[1])
        if self.test:
            # Increase replica and subscription count for this dataset
            self.subscriptions[site].append(dsn)
            self.datasetnum[dsn].append(site)
            self.logger.warning("No real rule added")
            if self.email:
                self.emailbody += '%s:%s %s %s\n' % (dsn[0], dsn[1], source, destsite)
            return True

        # This can fail if we try to recreate an existing SUSPENDED rule
        rule_id = None
        try:
            length = self.rucio.get_metadata(dsn[0], dsn[1])['length']
            if self.max_dataset_length is not None and length > self.max_dataset_length:
                self.logger.warning("Dataset %s:%s contains too many files: %d - skipping" % (dsn[0], dsn[1], length))
                if self.email:
                    self.emailbody += '%s:%s %s %s skipped for too many files\n' % (dsn[0], dsn[1], source, destsite)
            else:
                rule_id = self.rucio.add_replication_rule(dids,
                                                          1,
                                                          destsite,
                                                          weight=None,
                                                          lifetime=self.destlifetime,
                                                          grouping='DATASET',
                                                          account=self.account,  # will be set automatically
                                                          locked=self.destlocked,
                                                          source_replica_expression=source,
                                                          activity=self.activity,
                                                          notify='N',
                                                          comment=self.comment,
                                                          asynchronous=self.asynchronous)
                if rule_id:
                    self.donedatasets.addEntry(dsn[1])
        except DuplicateRule:
            self.logger.warning("Rule already exists")
        except DataIdentifierNotFound:
            self.logger.warning("Dataset %s:%s no longer exists", dsn[0], dsn[1])

        if not rule_id:
            rule_id = ['(no real rule)']
        elif self.email:
            self.emailbody += '%s:%s %s %s\n' % (dsn[0], dsn[1], source, destsite)

        self.logger.debug("Added rule id %s to %s from %s for %s:%s",
                          rule_id[0], destsite, source, dsn[0], dsn[1])

        # Increase replica and subscription count for this dataset
        self.subscriptions[site].append(dsn)
        self.datasetnum[dsn].append(site)
        return True


def main(args):
    """
    Main processing method
    """
    if len(args) != 2:
        logging.critical("Usage: RPG.py <config file>")
        sys.exit(1)
    rpg = None
    try:
        # Set up object for processing migration
        rpg = RPG(args[1])
        # Parse configuration
        rpg.parseConf()
        # Get info from CRIC about sites and blacklisting
        rpg.getCRICInfo()
        # Get potential datasets to rpg and current subscriptions
        rpg.getCurrentData()
        # Get the free space on destination sites
        rpg.getDestinationWeights()
        # Filter datasets which are already subscribed or were frozen too recently
        rpg.filterData()
        # Assign the datasets to tape sites and make subscriptions
        rpg.assignSites()

    except KeyboardInterrupt:
        pass
    except RPGException as e:
        logging.critical(str(e))
        # Send email report about RPG failure
        if rpg is not None:
            rpg.send_email(exception_str=str(e), config=args[1])
    else:
        # Send good email report
        rpg.send_email(config=args[1])


if __name__ == '__main__':
    main(sys.argv)
